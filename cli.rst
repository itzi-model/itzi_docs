Command line usage
------------------

Itzï is run from the command line.

Show the current version:

.. code:: sh

    $ itzi version

Display the command line options:

.. code:: sh

    $ itzi run -h
